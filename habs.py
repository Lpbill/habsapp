import requests
import json
import datetime
import time
import eel

# Constants for the HABS database
api_base_url = "https://statsapi.web.nhl.com/api/v1/"
api_teams_url = api_base_url + "teams/"
api_schedule_url = api_base_url + "schedule/"
api_standings_url = api_base_url + "standings/"
api_team_stats_url = api_teams_url + "stats/"

# For changing team id for fun
teamId = 8

lastGameDate = "2024-04-18"
firstGameDate = "2023-10-04"
class HABS:
    def __init__(self):
        self.stats = {
            "teamId": teamId,
            "teamName": "Montreal Canadiens",
            "wins": 0,
            "losses": 0,
            "ot": 0,
            "points": 0,
            "conferenceRank": 0,
        }
        self.easternStandings = []
        self.nextGame = Game()
        self.lastGame = Game()
        self.currentGame = Game()
        self.advancedStats = {
            "goalsPerGame": 0,
            "goalsAgainstPerGame": 0,
            "powerPlayPercentage": 0,
            "penaltyKillPercentage": 0,
            "shotsPerGame": 0,
            "shotsAllowed": 0,
            "winScoreFirst": 0,
            "winOppScoreFirst": 0,
            "winLeadFirstPer": 0,
            "winLeadSecondPer": 0,
            "faceOffWinPercentage": 0,
        }

    def __str__(self):
        return "Team: " + self.stats["teamName"] + "\n" + "Wins: " + str(self.stats["wins"]) + "\n" + "Losses: " + str(self.stats["losses"]) + "\n" + "Overtime Losses: " + str(self.stats["ot"]) + "\n" + "Points: " + str(self.stats["points"]) + "\n" + "Next Game: " + self.nextGame.gameDate + "\n" + "Last Game: " + self.lastGame.gameDate + "\n" + "Current Game: " + self.currentGame.gameDate + "\n" + "conferenceRank: " + str(self.stats["conferenceRank"]) + "\n" + "Goals per game: " + str(self.advancedStats["goalsPerGame"]) + "\n" + "Goals against per game: " + str(self.advancedStats["goalsAgainstPerGame"]) + "\n" + "Power play percentage: " + str(self.advancedStats["powerPlayPercentage"]) + "\n" + "Penalty kill percentage: " + str(self.advancedStats["penaltyKillPercentage"]) + "\n" + "Shots per game: " + str(self.advancedStats["shotsPerGame"]) + "\n" + "Shots allowed: " + str(self.advancedStats["shotsAllowed"]) + "\n" + "Win score first: " + str(self.advancedStats["winScoreFirst"]) + "\n" + "Win opp score first: " + str(self.advancedStats["winOppScoreFirst"]) + "\n" + "Win lead first per: " + str(self.advancedStats["winLeadFirstPer"]) + "\n" + "Win lead second per: " + str(self.advancedStats["winLeadSecondPer"]) + "\n" + "Face off win percentage: " + str(self.advancedStats["faceOffWinPercentage"])

    def getStats(self):
        # parse the stats from the api and store them. They come in a json format
        response_team_stats = requests.get(api_teams_url + str(self.stats["teamId"]) + "/stats")
        response_team_stats_json = response_team_stats.json()
        self.stats["teamName"] = response_team_stats_json["stats"][0]["splits"][0]["team"]["name"]
        self.stats["wins"] = response_team_stats_json["stats"][0]["splits"][0]["stat"]["wins"]
        self.stats["losses"] = response_team_stats_json["stats"][0]["splits"][0]["stat"]["losses"]
        self.stats["ot"] = response_team_stats_json["stats"][0]["splits"][0]["stat"]["ot"]
        self.stats["points"] = response_team_stats_json["stats"][0]["splits"][0]["stat"]["pts"]
        return self

    def getStandings(self):
        # parse the standings from the api and store them. They come in a json format
        response_team_standings = requests.get(api_standings_url+"byConference")
        response_team_standings_json = response_team_standings.json()
        list_of_eastern_teams = []
        for team in response_team_standings_json["records"][0]["teamRecords"]:
            list_of_eastern_teams.append(team["team"]["name"])
            if team["team"]["id"] == teamId:
                self.stats["conferenceRank"] = team["conferenceRank"]
        self.easternStandings = list_of_eastern_teams
        return self

    def getNextGame(self):
        # parse the next game from the api and store it. They come in a json format
        date = datetime.datetime.now()
        date = date.strftime("%Y-%m-%d")
        nextGame = Game()
        nextGame.getGame(date, lastGameDate)
        if nextGame.status == "Final":
            date = datetime.datetime.now() + datetime.timedelta(days=1)
            date = date.strftime("%Y-%m-%d")
            nextGame.getGame(date, lastGameDate)
        self.nextGame = nextGame
        return self

    def getLastGame(self):
        # parse the last game from the api and store it. They come in a json format
        date = datetime.datetime.now()
        date = date.strftime("%Y-%m-%d")
        lastGame = Game()
        lastGame.getGame(firstGameDate, date)
        self.lastGame = lastGame
        return self

    def getCurrentGame(self):
        # parse the current game from the api and store it. They come in a json format
        currentGame = Game()
        currentGame.getLiveGame()
        self.currentGame = currentGame
        return self

    def getAdvancedStats(self):
        response_team_stats = requests.get(api_teams_url + str(self.stats["teamId"]) + "/stats")
        response_team_stats_json = response_team_stats.json()
        self.advancedStats["goalsPerGame"] = response_team_stats_json["stats"][0]["splits"][0]["stat"]["goalsPerGame"]
        self.advancedStats["goalsAgainstPerGame"] = response_team_stats_json["stats"][0]["splits"][0]["stat"]["goalsAgainstPerGame"]
        self.advancedStats["powerPlayPercentage"] = response_team_stats_json["stats"][0]["splits"][0]["stat"]["powerPlayPercentage"]
        self.advancedStats["penaltyKillPercentage"] = response_team_stats_json["stats"][0]["splits"][0]["stat"]["penaltyKillPercentage"]
        self.advancedStats["shotsPerGame"] = response_team_stats_json["stats"][0]["splits"][0]["stat"]["shotsPerGame"]
        self.advancedStats["shotsAllowed"] = response_team_stats_json["stats"][0]["splits"][0]["stat"]["shotsAllowed"]
        self.advancedStats["winScoreFirst"] = response_team_stats_json["stats"][0]["splits"][0]["stat"]["winScoreFirst"]
        self.advancedStats["winOppScoreFirst"] = response_team_stats_json["stats"][0]["splits"][0]["stat"]["winOppScoreFirst"]
        self.advancedStats["winLeadFirstPer"] = response_team_stats_json["stats"][0]["splits"][0]["stat"]["winLeadFirstPer"]
        self.advancedStats["winLeadSecondPer"] = response_team_stats_json["stats"][0]["splits"][0]["stat"]["winLeadSecondPer"]
        self.advancedStats["faceOffWinPercentage"] = response_team_stats_json["stats"][0]["splits"][0]["stat"]["faceOffWinPercentage"]
        return self
        
class Game:
    def __init__(self):
        self.gameId = 0
        self.status = "status"
        self.gameDate = "aaaa-mm-dd"
        self.Broadcasts = []
        self.homeTeamID = 0
        self.homeTeam = "Home Team"
        self.awayTeamID = 0
        self.awayTeam = "Away Team"
        self.StartTime = "00:00"
        self.Live = False
        self.LiveScore = "0-0"
        self.FinalScore = "0-0"

    def __str__(self):
        return "Game ID: " + str(self.gameId) + "\nStatus: " + self.status + "\nGame Date: " + self.gameDate + "\nBroadcasts: " + str(self.Broadcasts) + "\nHome Team ID: " + str(self.homeTeamID) + "\nHome Team: " + self.homeTeam + "\nAway Team ID: " + str(self.awayTeamID) + "\nAway Team: " + self.awayTeam + "\nStart Time: " + self.StartTime + "\nLive: " + str(self.Live) + "\nLive Score: " + self.LiveScore + "\nFinal Score: " + self.FinalScore

    def getGame(self, lowerDate, upperDate):
        indice = 0
        response_schedule = requests.get(api_schedule_url + "?teamId="+str(teamId)+"&startDate=" + lowerDate + "&endDate=" + upperDate + "&expand=schedule.broadcasts.all")
        response_schedule_json = response_schedule.json()
        # if the lowerdate is the first game of the season, then we take the last game in the json
        if lowerDate == firstGameDate:
            indice = len(response_schedule_json["dates"]) - 1
            while response_schedule_json["dates"][indice]["games"][0]["status"]["detailedState"] != "Final":
                indice = indice - 1
        else:
            indice = 0

        self.status = response_schedule_json["dates"][indice]["games"][0]["status"]["detailedState"]
        self.gameDate = response_schedule_json["dates"][indice]["date"]
        self.gameId = response_schedule_json["dates"][indice]["games"][0]["gamePk"]
        self.Broadcasts = []
        for broadcast in response_schedule_json["dates"][indice]["games"][0]["broadcasts"]:
            self.Broadcasts.append(broadcast["name"])
        self.homeTeamID = response_schedule_json["dates"][indice]["games"][0]["teams"]["home"]["team"]["id"]
        self.homeTeam = response_schedule_json["dates"][indice]["games"][0]["teams"]["home"]["team"]["name"]
        self.awayTeamID = response_schedule_json["dates"][indice]["games"][0]["teams"]["away"]["team"]["id"]
        self.awayTeam = response_schedule_json["dates"][indice]["games"][0]["teams"]["away"]["team"]["name"]
        self.StartTime = response_schedule_json["dates"][indice]["games"][0]["gameDate"]
        self.Live = response_schedule_json["dates"][indice]["games"][0]["status"]["detailedState"] == "In Progress"
        if self.Live:
            self.LiveScore = str(response_schedule_json["dates"][indice]["games"][0]["teams"]["home"]["score"]) + "-" + str(response_schedule_json["dates"][0]["games"][0]["teams"]["away"]["score"])
            self.FinalScore = "n/a"
        else:
            self.LiveScore = "n/a"
            self.FinalScore = str(response_schedule_json["dates"][indice]["games"][0]["teams"]["home"]["score"]) + "-" + str(response_schedule_json["dates"][indice]["games"][0]["teams"]["away"]["score"])
        return self

    def getLiveGame(self):
        response_schedule = requests.get(api_schedule_url + "?teamId="+str(teamId)+"&expand=schedule.broadcasts.all")
        response_schedule_json = response_schedule.json()
        # si dates est vide, alors il n'y a pas de match en cours
        if len(response_schedule_json["dates"]) == 0:
            return -1
        else:
            self.Live = response_schedule_json["dates"][0]["games"][0]["status"]["detailedState"] == "In Progress"
            if not self.Live:
                return -1
            self.gameDate = response_schedule_json["dates"][0]["date"]
            self.gameId = response_schedule_json["dates"][0]["games"][0]["gamePk"]
            for broadcast in response_schedule_json["dates"][0]["games"][0]["broadcasts"]:
                self.Broadcasts.append(broadcast["name"])
            self.homeTeamID = response_schedule_json["dates"][0]["games"][0]["teams"]["home"]["team"]["id"]
            self.awayTeamID = response_schedule_json["dates"][0]["games"][0]["teams"]["away"]["team"]["id"]
            self.homeTeam = response_schedule_json["dates"][0]["games"][0]["teams"]["home"]["team"]["name"]
            self.awayTeam = response_schedule_json["dates"][0]["games"][0]["teams"]["away"]["team"]["name"]
            self.StartTime = response_schedule_json["dates"][0]["games"][0]["gameDate"]
            self.LiveScore = str(response_schedule_json["dates"][0]["games"][0]["teams"]["home"]["score"]) + "-" + str(response_schedule_json["dates"][0]["games"][0]["teams"]["away"]["score"])
            self.FinalScore = "n/a"
            return self

eel.init('interface')
# create a habs object
habs = HABS()

@eel.expose
def get_all_stats():
    print("get_all_stats")
    # get the next game date
    habs.getStats()
    habs.getAdvancedStats()
    habs.getNextGame()
    habs.getCurrentGame()
    habs.getLastGame()
    habs.getStandings()
    # print the habs class in the console
    print(habs)
    print("-------------------------------")
    print("Standings")
    print("-------------------------------")
    place = 1
    for team in habs.easternStandings:
        print(str(place) + " - " + team)
        place += 1
    print("-------------------------------")
    print("Logs")
    print("-------------------------------")

@eel.expose
def get_next_game_date():
    print("get_next_game_date")
    return habs.nextGame.gameDate

@eel.expose
def get_next_teams():
    print("get_next_teams")
    teams = []
    teams.append(habs.nextGame.homeTeam)
    teams.append(habs.nextGame.awayTeam)
    return teams

@eel.expose
def get_next_game_time():
    print("get_next_game_time")
    # from aaaa-mm-ddThh:mm:ssZ to hh:mm
    time = habs.nextGame.StartTime[11:16]
    # change the time zone from UTC to EST
    time = datetime.datetime.strptime(time, "%H:%M")
    time = time - datetime.timedelta(hours=4)
    time = datetime.datetime.strftime(time, "%H:%M")
    return time

@eel.expose
def get_next_broadcasts():
    print("get_next_broadcasts")
    # create a broadcasts string
    broadcasts = ""
    for broadcast in habs.nextGame.Broadcasts:
        broadcasts += broadcast + ", "

    # remove the last ", "
    broadcasts = broadcasts[:-2]
    return broadcasts

@eel.expose
def get_next_team_id():
    print("get_next_team_id")
    if habs.nextGame.homeTeamID == teamId:
        return habs.nextGame.awayTeamID
    else:
        return habs.nextGame.homeTeamID

@eel.expose
def get_last_game_date():
    print("get_last_game_date")
    return habs.lastGame.gameDate

@eel.expose
def get_last_team_id():
    print("get_last_team_id")
    if habs.lastGame.homeTeamID == teamId:
        return habs.lastGame.awayTeamID
    else:
        return habs.lastGame.homeTeamID

@eel.expose
def get_last_teams():
    print("get_last_teams")
    teams = []
    teams.append(habs.lastGame.homeTeam)
    teams.append(habs.lastGame.awayTeam)
    return teams

@eel.expose
def get_last_final_score():
    print("get_last_final_score")
    return habs.lastGame.FinalScore
    
@eel.expose
def get_live_status():
    print("get_live_status")
    return habs.currentGame.Live

@eel.expose
def get_live_start_time():
    print("get_live_start_time")
    # from aaaa-mm-ddThh:mm:ssZ to hh:mm
    time = habs.currentGame.StartTime[11:16]
    # change the time zone from UTC to EST
    time = datetime.datetime.strptime(time, "%H:%M")
    time = time - datetime.timedelta(hours=4)
    time = datetime.datetime.strftime(time, "%H:%M")
    return time

@eel.expose
def get_live_teams():
    print("get_live_teams")
    teams = []
    teams.append(habs.currentGame.homeTeam)
    teams.append(habs.currentGame.awayTeam)
    return teams

@eel.expose
def get_live_team_id():
    print("get_live_team_id")
    if habs.currentGame.homeTeamID == teamId:
        return habs.currentGame.awayTeamID
    else:
        return habs.currentGame.homeTeamID

@eel.expose
def get_live_score():
    print("get_live_score")
    return habs.currentGame.LiveScore

@eel.expose
def get_live_broadcasts():
    print("get_live_broadcasts")
    # create a broadcasts string
    broadcasts = ""
    for broadcast in habs.currentGame.Broadcasts:
        broadcasts += broadcast + ", "

    # remove the last ", "
    broadcasts = broadcasts[:-2]
    return broadcasts

@eel.expose
def get_wins_losses_ot_points():
    print("get_WLOTP_stats")
    stats = []
    stats.append(habs.stats["wins"])
    stats.append(habs.stats["losses"])
    stats.append(habs.stats["ot"])
    stats.append(habs.stats["points"])
    stats.append(habs.stats["conferenceRank"])
    return stats


eel.start('index.html', size=(1000, 600))
